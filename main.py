import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

modes = {'A': 0, 'y': 1}

def re(ds, dl, m):
    return np.sqrt((ds - dl) / (ds * dl) * (4 * G * m / c ** 2)) * dl

def te(re, vp):
    return re / vp

def u(t, u0, t0, te):
    return np.sqrt(u0 ** 2 + ((t-t0) / te) ** 2)

def a(u):
    return (u**2 + 2) / (u * np.sqrt(u**2 + 4))

def ym(u):
    return u/2 - np.sqrt(1 + u ** 2 / 4)

def yp(u):
    return u/2 + np.sqrt(1 + u ** 2 / 4)

def graph(X, u0, t0, te):
    YP = yp(u(X, u0, t0, te))
    YM = ym(u(X, u0, t0, te))
    return (X*(YM/np.sqrt(X**2+u0**2)), u0*(YM/np.sqrt(X**2+u0**2))),\
        (X*(YP/np.sqrt(X**2+u0**2)), u0*(YP/np.sqrt(X**2+u0**2)))

mode = sys.argv[1]
args = dict(map(lambda x: [(tmp:=x.split('='))[0], float(tmp[1])], sys.argv[2:]))
try:
    u0, t0, te, tl, tr, log = args['u0'], args['t0'], args['te'], args['tl'], args['tr'], args['log']
except KeyError:
    u0, t0, te = args['u0'], args['t0'], te(re(args['ds'], args['dl'], args['m']), args['vp'])
X = np.linspace(tl, tr, 500)
plt.style.use('seaborn-v0_8')
plt.grid(visible=1)
plt.rcParams.update({
    "text.usetex": True,
})
# plt.plot(X, np.linspace(5, 5, 50))
if modes[mode] == 0:
    A = a(u(X, u0, t0, te))
    if log:
        plt.gca().invert_yaxis()
        plt.plot((X-t0)/te, -2.5*np.log(A))
    else:
        plt.plot((X-t0)/te, A)
    plt.xlabel(r'$(t - t_0) / t_E$')
    plt.ylabel('Amplification' if not log else 'Magnification')
    # plt.gca().set_aspect('equal')
else:
    (mx, my), (px, py) = graph(X, u0, t0, te)
    plt.plot(mx, my)
    plt.plot(px, py)
    plt.gca().add_patch(plt.Circle((0,0), te, fill=0))
    plt.gca().set_aspect('equal')
plt.show()
