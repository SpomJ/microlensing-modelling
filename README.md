# Моделирование микролинзирования.

## Требования

Используются `python3`, библиотеки `matplotlib` и `numpy`.
```
pip3 install matplotlib numpy
```

## Установка

```sh
git clone https://gitlab.com/SpomJ/microlensing-modelling.git
cd microlensing-modelling
chmod +x main.py
```

## Запуск

```
python3 main.py [mode] [params: n=val]

mode:
	A: Amplification
	y: Object position

params:
	tl: left border
	tr: right border
	u0: minimal angle
	t0: event start
	te: einstein ring something something
```
	
